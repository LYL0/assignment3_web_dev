/*Louisa Yuxin Lin 1933472 Assignment3*/
"use strict"
document.addEventListener("DOMContentLoaded",setup);

function setup(){
	const btn = document.querySelector('#btn');
	btn.addEventListener('click',getQuote);
}
let quotes=document.querySelector('#quotes');
/*
**
*@this would get the quote from the source website.
*/
function getQuote(e){
	let promise="https://ron-swanson-quotes.herokuapp.com/v2/quotes";
	fetch(promise)
	.then(response => {
		if (response.ok){
			return response.json();
		}
		else{
			throw new Error("status code" +response.status)
		}
	})
	.then(json => display(json))
	.catch(error => treatError(error))	
}
/**
** @this would display the quote on the page.
*/
function display(json){
	let errorpara = document.querySelector("#errorpara");
	errorpara.style.visiblity="hidden";
	let para = document.querySelector("#para");
	para.style.visiblity="visible";
	para.textContent=json;
	
}
/**
** @this would deal with any potential errors.
*/
function treatError(error){
	console.error(error);
	let errorpara=document.querySelector('#errorpara');
	errorpara.textContent=error;
}